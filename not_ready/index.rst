* `Appendix B <app_b.html>`__ *Making Graphs with* ``matplotlib``

.. toctree::
    :maxdepth: 1
    :hidden:

    app_b.rst
