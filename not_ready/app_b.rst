..  Copyright (C) 2023 Jeffrey Elkner.  Permission is granted to copy,
    distribute and/or modify this document under the terms of the
    Creative Commons Attribution-ShareAlike 4.0 International License

.. _making_graphs_with_matplotlib:

Making Graphs with ``matplotlib``
=================================

