# Beginning Python Programming for Aspiring Web Developers

A textbook for beginners who want to learn Python programming for the purpose
of creating web applications with the Django web framework.


## Resources

* [Sphinx PyScript](https://sphinx-pyscript.readthedocs.io/en/latest/)
