Preface
=======

This book is specifically written to be a resource for our students at the
`Arlington Career Center <https://careercenter.apsva.us/>`_ who are taking
dual-enrolled computer science and web development courses with us.

| Jeffrey Elkner
| Arlington Career Center
| June, 2024
