..  Copyright (C) Jeffrey Elkner.  Permission is granted to copy, distribute
    and/or modify this document under the terms of the GNU Free Documentation
    License, Version 1.3 or any later version published by the Free Software
    Foundation; with Invariant Sections being Forward, Prefaces, and
    Contributor List, no Front-Cover Texts, and no Back-Cover Texts.  A copy of
    the license is included in the section entitled "GNU Free Documentation
    License".


.. _ch06_case_study:

Case Study: tree.py 
===================

The following program implements a subset of the behavior of the Unix `tree
<http://en.wikipedia.org/wiki/Tree_(Unix)>`__ program.

.. literalinclude:: tree.py 

Using the ``tree.py`` program above as a model, write a program named
``litter.py`` that creates an empty file named ``trash.txt`` in each
subdirectory of a directory tree given the root of the tree as an argument (or
the current directory as a default). Now write a program named ``cleanup.py``
that removes all these files.
