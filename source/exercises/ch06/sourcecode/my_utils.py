def count_chars(text):
    """
      >>> count_chars('')
      {}
      >>> count_chars('A')
      {'A': 1}
      >>> counts = count_chars('ThiS String has Upper and lower case Letters.')
      >>> counts['T']
      1
      >>> counts['i']
      2
      >>> counts[' ']
      7
    """
    counts = {}
    for ch in text:
        counts[ch] = counts[ch] + 1 if ch in counts else 1
    return counts


if __name__ == '__main__':
    import doctest
    doctest.testmod()
