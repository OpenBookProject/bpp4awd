import string
from my_utils import count_chars

s = input('Please enter a string: ')

counts = count_chars(s)

for ch in string.ascii_lowercase:
    if ch in counts or ch.upper() in counts:
        print(f'{ch} {counts.get(ch, 0) + counts.get(ch.upper(), 0)}')
