..  Copyright (C) Jeffrey Elkner.  Permission is granted to copy, distribute
    and/or modify this document under the terms of the GNU Free Documentation
    License, Version 1.3 or any later version published by the Free Software
    Foundation; with Invariant Sections being Forward, Prefaces, and
    Contributor List, no Front-Cover Texts, and no Back-Cover Texts.  A copy of
    the license is included in the section entitled "GNU Free Documentation
    License".


.. _ch03s00:

Chapter 3 Exercise Set 0: Chapter Review
========================================

"Being the computer"
--------------------

1. Enter each of the following *expressions* into the Python REPL to see how
   Pyton evaluates it. You should anticipate what you will see before each one.
   When you can "be the computer" and correctly predict what you will see
   before you press enter, you will have learned.

   * >>> "NVCC CSC221"[5:8]
   * >>> type([1, 'two', 3.0][1])
   * >>> 'a' in 'apple'
   * >>> [1, 2, 3, 4, 5, 6][2:-1]
   * >>> type(len('Mississippi'))
   * >>> for i in 2, 4, 6, 8:
     ...     print(i ** 2)
     ...
   * >>> 3 > 2 and 1 > 2
   * >>> not True or False
   * >>> not False and True 
   * >>> False and undefined_variable_name
   * >>> False or undefined_variable_name

2. What will be the Python shell's response to the following:

   .. sourcecode:: python

       >>> thing = ['a', 'b']
       >>> thing.append(['c', 'd']) 
       >>> thing


Syntax and operations
---------------------

What is the result of each of the following?


3. Add a line between each line starting with a python prompt with the value
   that would appear when the given expression is evaluated.

   .. sourcecode:: python

       >>> 'Python'[1]
       >>> "Strings are sequences of characters."[5]
       >>> len("wonderful")
       >>> 'Mystery'[:4]
       >>> 'p' in 'Pineapple'
       >>> 'apple' in 'Pineapple'
       >>> 'pear' not in 'Pineapple'
       >>> (2, 4, 6, 8, 10)[1:4]
       >>> [("cheese", "queso"), ("red", "rojo")][1][0][2]

4. You'll need to become familiar with the different methods of each type to
   do these.

   .. sourcecode:: python

       >>> 'Python'.upper()
       >>> 'Mississippi'.count('s') 
       >>> letters = ['c', 'z', 'b', 's']
       >>> letters.sort()
       >>> letters
       >>> (3, 9, 8, 42, 17).index(42)
       >>> "\t   \n     Don't pad me!   \n   \n".strip()
       >>> mystery = 'apples pears bananas cheesedoodles'.split()
       >>> mystery
       >>> mystery.sort() 
       >>> mystery
       >>> mystery.reverse() 
       >>> mystery

5. What value will appear after these two lines are entered at the prompt?

   .. sourcecode:: python

       >>> word = "Pneumonoultramicroscopicsilicovolcanoconiosis"
       >>> (word[6] + word[30] + word[33] + word[15]).upper()


Logical opposites
-----------------

6. Give the logical opposites of these conditions
    
   * ``a > b`` 
   * ``a >= b``
   * ``a >= 18 and day == 3``
   * ``a >= 18 and day != 3``
   * ``3 == 3``
   * ``3 != 3``
   * ``3 >= 4``
   * ``not (3 < 4)``


Four friends and a movie
------------------------

7. Write a program in a file named `movie_friends.py` that will produce a
   session something like this:

   .. sourcecode:: pycon

       $ python3 movie_friends.py

       Hmmm... You have 5 tickets to that new movie everyone wants to see.
       Whom should you invite to go with you?

       Enter the name of friend one: Sean 
       Your invite list now contains: ['Sean']

       Enter the name of friend two: Jonathan 
       Your invite list now contains: ['Sean', 'Jonathan']

       Enter the name of friend three: Margot 
       Your invite list now contains: ['Sean', 'Jonathan', 'Margot']

       Enter the name friend four: Justin 
       Your invite list now contains: ['Sean', 'Jonathan', 'Margot', 'Justin']

       Great!  You are ready to go to the movie...

       $


.. _what_is_is:

What ``is`` is 
--------------

8. What will be the output of the following program?

   .. sourcecode:: python
    
       this = ['I', 'am', 'not', 'a', 'crook']
       that = ['I', 'am', 'not', 'a', 'crook']
       print("Test 1:", this is that)
       that = this
       print("Test 2:", this is that)

   Provide a *detailed* explaination of the results.


What are you learning?
----------------------

9. Write a program in a file named `learning_what.py` that will produce a
   session something like this:

   .. sourcecode:: pycon

       $ python3 learning_what.py

       So, tell me one thing you are learning in that course: HTML 
       Your list of skills now contains: ['HTML']

       Name another thing you are learning: CSS 
       Your list of skills now contains: ['HTML', 'CSS']

       And another: Python 
       Your list of skills now contains: ['HTML', 'CSS', 'Python']

       Wow!  I should take that class too. 

       $


More syntax and operations
--------------------------

11. How would the Python interpreter respond to each of the following
    if it was entered at the Python prompt?

    * ``'NVCC Rocks!'[5]``
    * ``"Strings are sequences of characters."[-2]``
    * ``len("What's all this then? Amen!")``
    * ``'Mystery'[:4]``
    * ``'x' in 'Aardvark'``
    * ``'pin' in 'Pinapple'``
    * ``'a' not in 'Pinapple'``
    * ``(2, 3, 5, 7, 11, 13, 17)[-3:]``
    * ``[("cheese", "red", "sing"), ("queso", "red", "cantar")][0][2][0]``

12. You'll need to become familiar with the different methods of each type to
    do these.

    * ``'Python'.upper()``
    * ``'We are all in this together!'.count('e')``
    * ``(2, 3, 5, 7, 11, 13, 17, 19, 23).index(7)``
    * ``"\t   \n     Just the facts, mam!   \n   \n".strip()``

13. What will the Python shell print out for ``numbers`` in the following shell
    session?

    .. sourcecode:: python

        >>> numbers = [11, 7, 42, -3, 0, 18]
        >>> numbers.sort()
        >>> numbers


14. What will the Python shell print out for ``nomystery`` each time it is
    entered  in the following shell session?

    .. sourcecode:: python

        >>> nomystery = "These are the times that try men's souls".split()
        >>> nomystery
        >>> nomystery.sort() 
        >>> nomystery
        >>> nomystery.reverse() 
        >>> nomystery
