def doit(val):
    for x in range(-1, 2):
        if 2 * x < 4:
            val += 1
    else:
        val += 2
    return val

print(doit(-2))
