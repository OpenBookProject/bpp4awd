def make_it_happen(thing1, thing2):
    s = '*'
    while thing1 != thing2 and thing1:
        thing1 //= 2
        s += '*' 
    else:
        s += '!' 
    return s

print(make_it_happen(16, 2))
