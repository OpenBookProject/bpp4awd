..  Copyright (C) Jeffrey Elkner, Kevin Reed, Chris Meyers, and Dario Mitchell.
    Permission is granted to copy, distribute and/or modify this document under
    the terms of the GNU Free Documentation License, Version 1.3 or any later
    version published by the Free Software Foundation; with Invariant Sections
    being Forward, Prefaces, and Contributor List, no Front-Cover Texts, and no
    Back-Cover Texts.  A copy of the license is included in the section
    entitled "GNU Free Documentation License".


Chapter 2 Exercise Set 1 Solutions
==================================

w1 = 'All'
w2 = 'work'
w3 = 'and'
w4 = 'no'
w5 = 'play'
w6 = 'makes'
w7 = 'Jack'
w8 = 'a'
w9 = 'dull'
w10 = 'boy.'
print(w1, w2, w3, w4, w5, w6, w7, w8, w9, w10)
