def copy_matrix(matrix):
    """
      >>> copy_matrix([[1, 2], [3, 4]])
      [[1, 2], [3, 4]]
      >>> copy_matrix([[1, 2, 3], [4, 5, 6]])
      [[1, 2, 3], [4, 5, 6]]
      >>> copy_matrix([[1, 2], [3, 4], [5, 6], [7, 8]])
      [[1, 2], [3, 4], [5, 6], [7, 8]]
      >>> m = [[1, 0, 0], [0, 2, 0], [0, 0, 3]]
      >>> cm = copy_matrix(m)
      >>> cm
      [[1, 0, 0], [0, 2, 0], [0, 0, 3]]
      >>> for row_num, row in enumerate(cm):
      ...     for col_num, col_val in enumerate(row):
      ...         cm[row_num][col_num] = 42
      ...
      >>> cm
      [[42, 42, 42], [42, 42, 42], [42, 42, 42]]
      >>> m
      [[1, 0, 0], [0, 2, 0], [0, 0, 3]]
    """
    matrix_copy = []

    for row_num, row in enumerate(matrix):
        matrix_copy.append([])
        for col in row:
            matrix_copy[row_num].append(col)

    return matrix_copy


if __name__ == '__main__':
    import doctest
    doctest.testmod()
