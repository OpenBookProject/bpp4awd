invitees = []

while True:
    invitee = input("Enter invitee's name (or just enter to finish): ")
    if not invitee:
        break
    invitees.append(invitee)

print()
for invitee in invitees:
    print(invitee + ", please attend our party this Saturday!")
