s = """\n{name} was beginning to get very {adj} of {verb} by her {relative} on
the {noun}, and of having nothing to do: once or twice she had peeped into the
{noun2} her {relative} was {verb2}, but it had no {noun3} or {noun4} in it,
‘and what is the use of a {noun2},’ thought {name} ‘without {noun3}s or
{noun4}s?’"""

parts = {'name': "girl's name", 'adj': 'adjective',
         'verb': "verb ending in 'ing'", 'relative': 'kind of relative',
         'noun': 'noun', 'noun2': 'noun', 'verb2': "verb ending in 'ing'",
         'noun3': 'noun', 'noun4': 'noun'}

for part in parts.keys():
    val = input("Please enter a {0}: ".format(parts[part]))
    parts[part] = val

print(s.format(**parts))
