def lots_of_letters(word):
    """
      >>> lots_of_letters('Lidia')
      'Liidddiiiiaaaaa'
      >>> lots_of_letters('Python')
      'Pyyttthhhhooooonnnnnn'
      >>> lots_of_letters('')
      ''
      >>> lots_of_letters('1')
      '1'
    """
    result_str = ''
    i = 0
    numletters = len(word)

    while i < numletters:
        result_str += (i + 1) * word[i]
        i += 1

    return result_str


if __name__ == '__main__':
    import doctest
    doctest.testmod()
