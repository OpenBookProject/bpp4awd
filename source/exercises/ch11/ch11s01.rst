..  Copyright (C) Jeffrey Elkner.  Permission is granted to copy, distribute
    and/or modify this document under the terms of the GNU Free Documentation
    License, Version 1.3 or any later version published by the Free Software
    Foundation; with Invariant Sections being Forward, Prefaces, and
    Contributor List, no Front-Cover Texts, and no Back-Cover Texts.  A copy of
    the license is included in the section entitled "GNU Free Documentation
    License".


.. _ch11s01:

Chapter 11 exercise set 1
=========================

We will be using `Bottle <http://bottlepy.org>` micro
`web application framework <http://en.wikipedia.org/wiki/Web_framework>`_ to
write our web application in this book.  This set of exercises helps you get
started using bottle.


``hello_world.py`` in bottle
----------------------------

Create a directory named ``BottleApps`` in your home directory.  Make a sub
directory of that named ``HelloWorld``.  Inside that directory create a
file named ``hello_world_bottle.py`` with the following content:

.. sourcecode:: python3

    from bottle import route, run

    @route('/hello/:name')
    def index(name='World')
        return '<b>Hello %s!</b>' % name

    run(host='0.0.0.0', port=8080)

.. note::

    If you are on a shared server, you may need to use a port other than
    ``8080`` to make sure the web server you start isn't listening on the same
    port as someone else on the same machine.

    If you are the only one on your machine, of course, this won't be an issue.
