Contributor List
================

To paraphrase the philosophy of the Free Software Foundation, this book is free
like free speech, but not necessarily free like free pizza. It came about
because of a collaboration that would not have been possible without the `Free
Software Foundation <https://www.fsf.org/>`_, which developed the copyright
license under which the original version of the book that is the direct
ancenstor to this one, `How to Think Like a Computer Scientist: Learning with
Python <http://www.greenteapress.com/thinkpython/thinkCSpy.pdf>`_, was
licensed. So I would like to thank the Free Software Foundation for developing
that license.

I would also like to thank the many sharp-eyed and thoughtful readers who have
emailed suggestions and corrections. In the spirit of free software, I want to 
send out a heart felt "Thank you!" in the form this contributor list.

Unfortunately, this list is not complete.  It is not possible to include
everyone who sends in a typo or two. Those of you who have done so have my
gratitude, as well as the personal satisfaction of making a book you found
useful better for you and everyone else who uses it.

If you have a chance to look through the list, you should realize that each
person here has spared you and all subsequent readers from the confusion of a
technical error or a less-than-transparent explanation, just by sending me a
note.

There are undoubtedly still errors to be found. If you should come across one,
I hope you will take a minute to contact me. The email address is
`jeff@elkner.net <mailto:jeff@elkner.net>`__. Substantial changes made due to
your suggestions will add you to the next version of the contributor list
(unless you ask to be omitted). Thank you!


First Edition
~~~~~~~~~~~~~

* Thanks most of all to
  `Allen B. Downey <http://en.wikipedia.org/wiki/Allen_B._Downey>`__, author of
  the orginial version of How to Think Like a Computer Scientist, who started
  me on this journey working on free textbooks back in 1999.  It would be a
  gross understatement to say he had a huge impact on my life, since I wouldn't
  have been able to teach computer programming with Python were it not for
  having his book available to me to modify for my own needs.  Much of the text
  in this new version of the book is still Allen's.

* Thanks to collaborators Chris Meyers and Peter Wentworth.  Chris helped me
  understand many challenging concepts and presented wonderful examples
  for use in this book.  He definitely takes a joy in programming, as his
  `Python for Fun <http://openbookproject.net/py4fun/>`__ amply demonstrates.
  Peter spun off his
  `own version <http://openbookproject.net/thinkcs/python/english3e/>`__
  of the book, using Python 3, from which I have in turn taken material for
  use in this one.

* Thanks to `Charles Severance (a.k.a. Dr. Chuck) <http://www.dr-chuck.com/>`_,
  whose `Python for Everybody <https://www.py4e.com/>`_ (PY4E) provided the
  video lessons that helped get me through the shift to teaching online during
  the COVID pandemic. I plan to use both PY4E and `Django for Everybody
  <https://www.dj4e.com/>`_ as the guides to structuring this book as it
  evolves.

* Thanks to the students of the Northern Virginia Community College Summer
  2013 Web Design and Development program for being the first ones to use
  this book, and for lending their keen eyes to uncovering many errors.
  Tanishka Amin, Ochirnyam Baasandorj, Jacob Birnbaum, Parmvir Chahal,
  Owen Davies, Reid Fennerty, Patrick Gorman, Chris Hedrick, Sarah Mazur,
  Gerson Osorio, Sam Phillips, Shane Polisar, and Erfan Yakup, I can't begin
  to tell you all what a pleasure it was working with you all.  I've been
  teaching for over 20 years, and I don't think I've ever had a class that
  was your equal.

* Thanks also to the students of my Scientific Programming class of the
  Summer of 2014 for being the second group of students to use this book and
  the ones who helped me ready to release it to the public. Alejandro
  Asef-Sargent, Ed Barbeau, Harrison Crosse, Margot Hanclich, Kathryn Heaton,
  Elliott Kunkel, Justin Moore, Prisila Otazo-Ticona, Tristan Ritland, Sean
  Romiti-Schulze, George Ung, Peter Vasilopoulos, and Jonathan Williams, you
  each have my undying gratitude. George, Margot, and Harrison deserve special
  mention for finding errors in the book and for helping me teach the class.
  George provided a number of excellent projects for us to work on.  Some of
  these, such as connect4, will make it into the book as case studies.
