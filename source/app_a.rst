..  Copyright (C) 2024 Jeffrey Elkner.  Permission is granted to copy,
    distribute and/or modify this document under the terms of the
    Creative Commons Attribution-ShareAlike 4.0 International License

.. _configuring_debian_for_python_web_development:

Configuring Debian for Python Web Development
=============================================

    *Note:* the following instructions assume that you are connected to
    the Internet and that you have the standard Debian package repositories
    enabled.  All unix shell commands are assumed to be running from your home
    directory ($HOME).  Finally, any command that begins with ``sudo`` assumes
    that you have administrative rights on your machine.  If you do not ---
    please ask your system administrator about installing the software you
    need.

What follows are instructions for setting up an Debian 12 (bookworm) home
environment for use with this book. I use Debian for both development and
testing of the book, so it is the only system about which I can personally
answer setup and configuration questions.

In the spirit of software freedom and open collaboration, please contact me if
you would like to maintain a similar appendix for your own favorite system. I'd
be more than happy to link to it or put it on the Open Book Project site,
provided you agree to answer user feedback concerning it.

Thanks!

| `Jeffrey Elkner <mailto:jeff@elkner.net>`_
| Arlington, Virginia


.. _configuring-python3:

Python3
-------

While Debian bookworm only comes with Python 3 installed, the legacy of Python
2 is still with us. Typing ``python`` at the shell prompt will give a command
not found error.

Use the command ``python3`` for Python 3.

**Note**: You can fix this behavior by installing the ``python-is-python3``
package with::

    $ sudo apt install python-is-python3

The book examples assume you have done this.

In addition to the `debian packages
<https://en.wikipedia.org/wiki/Debian_package>`_ in the Debian package archive,
we will be using Python software from the `Python Package Index
<https://pypi.python.org/pypi>`_ or PyPI. The tool for installing packages from
PyPI is called `pip
<https://en.wikipedia.org/wiki/Pip_%28package_manager%29>`_.

To add this package run following from the unix command prompt::

    $ sudo apt install python3-pip

Now would also be a good time to install a few other packages you will want
to have on your system::

    $ sudo apt install python3-tk pycodestyle git 

This will install the `Tkinter <https://en.wikipedia.org/wiki/Tkinter>`_ GUI
toolkit, the `pep8 <https://www.python.org/dev/peps/pep-0008>`_ Python style
checker, and the `git <https://en.wikipedia.org/wiki/Git>`_ revision control
system which we will use to grab some program examples.


.. _installing-bottle:

Bottle
------

`Bottle <https://bottlepy.org>`_ is a micro `web application framework
<https://en.wikipedia.org/wiki/Web_application_framework>`_ written in Python.
It is used in this book to introduce
`web application <https://en.wikipedia.org/wiki/Web_application>`_ development.

To install ``bottle`` run::

    $ sudo apt install python3-bottle 

Then try:

.. sourcecode:: python3

    >>> import bottle

at the python prompt to varify that it is working.


.. _configuring-vim:

Vim
---

`Vim <https://www.vim.org>`_ can be used very effectively for Python
development, but Ubuntu only comes with the ``vim-tiny`` package installed by
default, so it doesn't support color syntax highlighting or auto-indenting.

To use Vim, do the following:

#. From the unix command prompt, run::

       $ sudo apt install vim

#. Create a file in your home directory named ``.vimrc`` that contains the
   following::

       syntax enable
       filetype indent on
       set et
       set sw=4
       set smarttab
       map <f3> :w\|!python % <cr>
       map <f4> :w\|!python -m doctest -v % <cr>
       map <f8> :w\|!pycodestyle % -v <cr>

When you edit a file with a ``.py`` extension, you should now have color systax
highlighting and auto indenting. Pressing the ``<f3>`` key should run your
program, and bring you back to the editor when the program completes.
``<f4>`` runs the program with the verbose (``-v``) switch set, which will be
helpful when running doctests. ``<f8>`` will run the pep8 style checker against
your program source, which is useful in helping you learn to write Python
programs with good styling.

To learn to use vim, run the following command at a unix command
prompt::

    $ vimtutor


.. _configuring-home:

`$HOME` environment
-------------------

The following creates a useful environment in your
`home directory <https://en.wikipedia.org/wiki/Home_directory>`_ for using
``pip3`` to install packages into your home directory and for adding your
own Python libraries and executable scripts:

#. From the command prompt in your home directory, create ``bin`` and ``lib``
   subdirectories of your ``.local`` directory by running the following
   command::

       $ mkdir .local/lib .local/bin 

#. Now add a ``my_python`` subdirectory to ``.local/lib``::

       $ mkdir .local/lib/my_python

#. Add the following lines to the bottom of your ``.bashrc`` in your home
   directory::

       EDITOR=vim
       PATH=$HOME/.local/bin$PATH
       PYTHONPATH=$HOME/.local/lib/my_python
    
       export EDITOR PATH PYTHONPATH

   This will set your prefered editor to Vim, add your own ``.local/bin``
   directory as a place to put executable scripts, and add
   ``.local/lib/my_python`` to your Python search path so modules you put there
   will be found by Python.

   Then run::

       $ . .bashrc

   to set these `environment varialbles
   <https://en.wikipedia.org/wiki/Environment_variable>`_ and prepend the
   ``.local/bin`` directory to your `search path
   <https://en.wikipedia.org/wiki/Path_(variable)>`_ (`note:` logging out and
   back in will accomplish the same result).


.. _installing-lumpy:

Lumpy
-----

Lumpy is python module that generates `UML
<https://en.wikipedia.org/wiki/Unified_Modeling_Language>`_ diagrams. It was
written by `Allen B. Downey <https://en.wikipedia.org/wiki/Allen_B._Downey>`_
as part of his `Swampy <http://www.greenteapress.com/thinkpython/swampy>`_
suite of Python programs written for use with his textbooks.

The version here is modified to work with Python 3 on Ubuntu 16.04.
Click on :download:`lumpy.py <resources/app_a/lumpy.py>` to download the module.
Put this file in your ``.local/lib/my_python`` directory after your
:ref:`configuring-home` is configured.

Lumpy is used in several of the exercises in this book to help illustrate
python `data structures <https://en.wikipedia.org/wiki/Data_structure>`_.


Making a python script executable and runnable from anywhere
------------------------------------------------------------

On unix systems, Python scripts can be made *executable* using the following
process:

#. Add this line as the first line in the script:

   .. sourcecode:: python
    
       #!/usr/bin/env python3

#. At the unix command prompt, type the following to make ``myscript.py``
   executable::

       $ chmod +x myscript.py

#. Move ``myscript.py`` into your ``.local/bin`` directory, and it will be
   runnable from anywhere.
