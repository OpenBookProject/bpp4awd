Beginning Python Programming
============================

.. image:: _static/bpp4awd.png
    :alt: BPP4AWD Logo

for Aspiring Web Developers
---------------------------

Using Python 3

by Jeff Elkner and Chris Jones
(with liberal borrowings from the work of Allen B. Downey, Peter Wentworth,
and Charles Severance)

illustrated by Natalia Cerna 

Last updated: 24 January 2025

* `Copyright Notice <copyright.html>`__ 
* `Contributor List <contrib.html>`__
* `Preface <preface.html>`__ 
* `Chapter 1 <ch01.html>`__ *Why program?*
* `Chapter 2 <ch02.html>`__ *Values, expressions and statements*
* `Chapter 3 <ch03.html>`__ *Strings, lists and tuples*
* `Chapter 4 <ch04.html>`__ *Conditionals, loops and exceptions*
* `Chapter 5 <ch05.html>`__ *Functions*
* `Chapter 6 <ch06.html>`__ *Dictionaries, sets, files and modules*
* `Chapter 7 <ch07.html>`__ *Classes and objects*
* `Chapter 8 <ch08.html>`__ *Inheritance*
* `Chapter 9 <ch09.html>`__ *Front-end web development*
* `Chapter 10 <ch10.html>`__ *Networking*
* `Chapter 11 <ch11.html>`__ *Server-side scripting*
* `Appendix A <app_a.html>`__ *Configuring Debian for Python web development*
* `Creative Commons Attribution-ShareAlike 4.0 International Licence
  <cc-by-sa-4.0.html>`__

.. toctree::
    :maxdepth: 1
    :hidden:

    copyright.rst
    contrib.rst
    preface.rst

.. toctree::
    :maxdepth: 1
    :numbered:
    :hidden:

    ch01.rst
    ch02.rst
    ch03.rst
    ch04.rst
    ch05.rst
    ch06.rst
    ch07.rst
    ch08.rst
    ch09.rst
    ch10.rst
    ch11.rst


.. toctree::
    :maxdepth: 1
    :hidden:

    app_a.rst
    cc-by-sa-4.0.rst


.. toctree::
    :maxdepth: 1
    :hidden:
    :glob:

    exercises/ch??/* 
    exercises/solutions/ch??/* 

* :ref:`search`
