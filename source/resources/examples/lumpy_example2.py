#!/usr/bin/python3

from lumpy import Lumpy
visualization = Lumpy()

# if you don't make a reference after instantiating
# Lumpy, the local variable lumpy is visible.  Even
# so, the Lumpy class is opaque by default, so we
# have to make it transparent.
visualization.transparent_class(Lumpy)

visualization.class_diagram()
