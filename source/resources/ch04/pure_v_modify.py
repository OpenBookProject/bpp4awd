def double_stuff_v1(a_list):
    index = 0
    for value in a_list:
        a_list[index] = 2 * value
        index += 1


def double_stuff_v2(a_list):
    new_list = []
    for value in a_list:
        new_list += [2 * value]
    return new_list
