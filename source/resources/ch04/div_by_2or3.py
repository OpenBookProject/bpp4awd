print("Num   Div by 2 and/or 3?")
print("---   ------------------")

for num in range(2, 21):
    if num % 2 == 0 and num % 3 == 0:
        result = "both"
    elif num % 2 == 0:
        result = "by 2"
    elif num % 3 == 0:
        result = "by 3"
    else:
        result = "neither"

    print("{0}\t     {1}".format(num, result)) 
