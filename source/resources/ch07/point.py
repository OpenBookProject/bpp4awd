class Point:
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    def __str__(self):
        return "({0}, {1})".format(self.x, self.y)


def distance(p1, p2):
    """
      >>> p1 = Point(1, 2)
      >>> p2 = Point(4, 6)
      >>> distance(p1, p2)
      5.0
      >>> p3 = Point(16, 11)
      >>> distance(p2, p3)
      13.0
    """
    return ((p1.x - p2.x) ** 2 + (p1.y - p2.y) ** 2) ** 0.5
