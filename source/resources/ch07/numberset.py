class NumberSet:
    """
      >>> nums = NumberSet([2, 4, 6])
      >>> print(nums)
      [2, 4, 6]
      >>> nums.mean()
      4.0
      >>> nums.median()
      4
      >>> nums.mode()
      [2, 4, 6]
      >>> nums2 = NumberSet([1, 2, 6, 6])
      >>> nums2.mean()
      3.75
      >>> nums2.median()
      4.0
      >>> nums2.mode()
      [6]
      >>> numset = [3, 5, 19, 42, 5, 42, 11]
      >>> nums3 = NumberSet(numset)
      >>> nums3.numlist
      [3, 5, 5, 11, 19, 42, 42]
      >>> numset
      [3, 5, 19, 42, 5, 42, 11]
      >>> round(nums3.mean())
      18
      >>> nums3.median()
      11
      >>> nums3.mode()
      [5, 42]
    """
    def __init__(self, numlist):
        self.numlist = numlist.copy()
        self.numlist.sort()

    def __str__(self):
        return str(self.numlist)

    def mean(self):
        return sum(self.numlist) / len(self.numlist)

    def median(self):
        mid = len(self.numlist) // 2

        if len(self.numlist) % 2 == 1:
            return self.numlist[mid]

        return sum(self.numlist[mid-1:mid+1]) / 2

    def mode(self):
        mode_list = []
        counts = {}

        for num in self.numlist:
            if num not in counts:
                counts[num] = 1
            else:
                counts[num] += 1

        max_freq = max(counts.values())

        for num in self.numlist:
            if counts[num] == max_freq and num not in mode_list:
                mode_list.append(num)

        return mode_list
