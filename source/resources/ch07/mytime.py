class Time:
    def __init__(self, hours=0, minutes=0, seconds=0):
        self.hours = hours
        self.minutes = minutes
        self.seconds = seconds

    def __str__(self):
        s = "{0}:{1:02d}:{2:02d}"
        return s.format(self.hours, self.minutes, self.seconds)
